//
//  HeartratesPlugIn.m
//  Heartrates
//
//  Created by Joris Kluivers on 3/10/13.
//  Copyright (c) 2013 Joris Kluivers. All rights reserved.
//

#import <IOBluetooth/IOBluetooth.h>

#import "HeartratesPlugIn.h"
#import "JKHRSensor.h"

#define	kQCPlugIn_Name				@"Heartrates"
#define	kQCPlugIn_Description		@"Connects to a hartrate monitor and gives access to person heartrate"
#define kQCPlugIn_Copyright         @"Joris Kluivers"

@interface HeartratesPlugIn () <CBCentralManagerDelegate>
@property(nonatomic, strong) CBCentralManager *manager;
@property(nonatomic, strong) CBPeripheral *peripheral;
@property(nonatomic, strong) JKHRSensor *sensor;
@property(nonatomic, readonly) dispatch_queue_t queue;
@property(nonatomic, assign) NSUInteger heartrate;
@end

@implementation HeartratesPlugIn {
    BOOL _shouldKeepRunning;
}

@dynamic outputHeartrate;

+ (BOOL)isSafe
{
    return YES;
}

+ (NSDictionary *)attributes
{
	return @{
        QCPlugInAttributeNameKey: kQCPlugIn_Name,
        QCPlugInAttributeDescriptionKey: kQCPlugIn_Description,
        QCPlugInAttributeCopyrightKey: kQCPlugIn_Copyright
    };
}

+ (NSDictionary *)attributesForPropertyPortWithKey:(NSString *)key
{
	if ([key isEqualToString:@"outputHeartrate"]) {
		return @{ QCPortAttributeNameKey: @"Heartrate" };
	}
	
	return nil;
}

+ (QCPlugInExecutionMode)executionMode
{
	return kQCPlugInExecutionModeProcessor;
}

+ (QCPlugInTimeMode)timeMode
{
	// Return the time dependency mode of the plug-in: kQCPlugInTimeModeNone, kQCPlugInTimeModeIdle or kQCPlugInTimeModeTimeBase.
	return kQCPlugInTimeModeIdle;
}

- (id)init
{
	self = [super init];
	if (self) {
        _queue = dispatch_queue_create("ble-queue", 0);
	}
	
	return self;
}

- (QCPlugInViewController *) createViewController
{
    return [[QCPlugInViewController alloc] initWithPlugIn:self viewNibName:@"PluginSettingsViewController"];
}

- (void) findAndConnectDevice
{
    NSLog(@"Find and connect");
    [self.manager scanForPeripheralsWithServices:@[
        [CBUUID UUIDWithString:@"180D"]
    ] options:nil];
}

- (void) stopScan
{
    if (self.sensor) {
        [self.manager cancelPeripheralConnection:self.sensor.peripheral];
        
        [self.sensor removeObserver:self forKeyPath:@"heartRate"];
        self.sensor = nil;
    }
	[self.manager stopScan];
}

#pragma mark - Central Manager

- (void) centralManagerDidUpdateState:(CBCentralManager *)central
{
    NSLog(@"Update state");
    if (central.state == CBCentralManagerStatePoweredOn) {
        [self findAndConnectDevice];
    }
}

- (void) centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    CBUUID *serviceUUID = [CBUUID UUIDWithString:@"180D"];
    
    NSArray *services = advertisementData[CBAdvertisementDataServiceUUIDsKey];
    if (![services containsObject:serviceUUID]) {
        return;
    }
    
    if (self.sensor) {
        // already connected
        return;
    }
    
    
    NSLog(@"Connect peripheral: %@", peripheral);
    // retain peripheral, or will fail without notice
    self.peripheral = peripheral;
    [self.manager connectPeripheral:peripheral options:nil];
}

- (void) centralManager:(CBCentralManager *)central didRetrievePeripherals:(NSArray *)peripherals
{
    NSLog(@"Retrieved: %@", peripherals);
}

- (void) centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    NSLog(@"Did connect sensor");
    
    self.sensor = [[JKHRSensor alloc] initWithPeripheral:peripheral];
    [self.sensor addObserver:self forKeyPath:@"heartRate" options:nil context:nil];
}

- (void) centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    NSLog(@"Disconnected");
    [self.sensor removeObserver:self forKeyPath:@"heartRate"];
    self.sensor = nil;
}

- (void) centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    NSLog(@"Failed to connect to sensor: %@", error);
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (self.sensor == object && [keyPath isEqualToString:@"heartRate"]) {
        NSUInteger heartrate = self.sensor.heartRate;
        
        self.heartrate = heartrate;
        
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (BOOL)startExecution:(id <QCPlugInContext>)context
{
    _manager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
//    dispatch_sync(dispatch_get_main_queue(), ^{
//        
//    });
//    
//    dispatch_queue_t current = dispatch_get_current_queue();
//    dispatch_async(_queue, ^{
//        NSRunLoop *theRL = [NSRunLoop currentRunLoop];
//        while (_shouldKeepRunning && [theRL runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]]);
//        
//        NSLog(@"Runloop done running");
//    });
	
	return YES;
}

- (BOOL)execute:(id <QCPlugInContext>)context atTime:(NSTimeInterval)time withArguments:(NSDictionary *)arguments
{
    
    self.outputHeartrate = self.heartrate;
    
    return YES;
}


- (void)stopExecution:(id <QCPlugInContext>)context
{
    NSLog(@"Stop execution");
    _shouldKeepRunning = NO;
	[self stopScan];
    _manager = nil;
}

@end
