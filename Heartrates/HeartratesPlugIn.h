//
//  HeartratesPlugIn.h
//  Heartrates
//
//  Created by Joris Kluivers on 3/10/13.
//  Copyright (c) 2013 Joris Kluivers. All rights reserved.
//

#import <Quartz/Quartz.h>

@interface HeartratesPlugIn : QCPlugIn

@property(assign) NSUInteger outputHeartrate;

@end
