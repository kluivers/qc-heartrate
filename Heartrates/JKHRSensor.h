//
//  JKHRSensor.h
//  Heartrates
//
//  Created by Joris Kluivers on 4/30/13.
//  Copyright (c) 2013 Joris Kluivers. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CBPeripheral;

@interface JKHRSensor : NSObject

@property(nonatomic, readonly) NSUInteger heartRate;
@property(nonatomic, strong) CBPeripheral *peripheral;

- (id) initWithPeripheral:(CBPeripheral *)peripheral;

@end
