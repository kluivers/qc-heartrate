# Heart rate Quartz Composer plugin

A custom patch to read the heart rate from a Bluetooth Smart peripheral.

This patch currently connects to the first peripheral found that publishes a [heart rate service](http://developer.bluetooth.org/gatt/services/Pages/ServiceViewer.aspx?u=org.bluetooth.service.heart_rate.xml).

## Contact

For questions or comments I'm available via:

- [@kluivers on Twitter](http://twitter.com/kluivers). 
- [contact-joris@kluivers.nl](mailto:contact-joris@kluivers.nl).


![Heart rate patch in a composition](https://bitbucket.org/kluivers/qc-heartrate/raw/master/composition.png)
